<?php

namespace Gong\WinnerFormBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function formAction($hash = '')
    {
        if(!$this->checkIfCorrectHash($hash)){
            return $this->redirect($this->get('router')->generate('gong_app_homepage'));
        }

        return $this->render('WinnerFormBundle:Default:form.html.twig', array('hash' => $hash));
    }

    public function saveFormAction(Request $request){

        $response   = new JsonResponse();
        $result     = array('success' => false);

        $hash       = $request->get('hash', '');

        /* sprawdzam czy poprawny hash */
        if(!$this->checkIfCorrectHash($hash)){
            $response->setData($result);
            return $response;
        }

        $firstName       = $request->get('firstname', '');
        $lastName        = $request->get('surname', '');
        $street          = $request->get('street', '');
        $hnumber         = $request->get('hnumber', '');
        $flat            = $request->get('flat', '');
        $zipCode         = $request->get('zipcode', '');
        $city            = $request->get('city', '');
        $phone           = $request->get('tel', '');
        $secondAddress   = $request->get('delivery_address', '');
        $street2         = $request->get('delivery_street', '');
        $hnumber2        = $request->get('delivery_hnumber', '');
        $flat2           = $request->get('delivery_flat', '');
        $zipCode2        = $request->get('delivery_zipcode', '');
        $city2           = $request->get('delivery_city', '');
        $revenue         = $request->get('usn', '');
        $revenueAddress  = $request->get('usadr', '');
        $pesel           = $request->get('peselnip', '');
        $clause1         = $request->get('agree', '');

        if($firstName == '' || $lastName == '' || $street == '' || $hnumber == '' || $zipCode == '' || $zipCode == '' || $city == '' || $phone == ''){
            $response->setData($result);
            return $response;
        }

        if($secondAddress == 'on' && ($street2 == '' || $hnumber2 == '' || $zipCode2 == '' || $city2 == '')){
            $response->setData($result);
            return $response;
        }

        if($revenue == '' || $revenueAddress == '' || $pesel == '' || $clause1 != 'on'){
            $response->setData($result);
            return $response;
        }

        $dm     =  $this->get('doctrine_mongodb')->getManager();
        $resultWinner =  $dm->getRepository($this->getParameter('winner_form.winner_document_name'))->findOneBy(array('hash' => $hash));

        $resultWinner->setFilledData(true);
        $resultWinner->setFormCity($city);
        $resultWinner->setFormFirstName($firstName);
        $resultWinner->setFormLastName($lastName);
        $resultWinner->setFormStreet($street);
        $resultWinner->setFormHnumber($hnumber);
        $resultWinner->setFormFlat($flat);
        $resultWinner->setFormZipCode($zipCode);
        $resultWinner->setFormPhone($phone);
        $resultWinner->setFormRevenue($revenue);
        $resultWinner->setFormRevenueAddress($revenueAddress);
        $resultWinner->setFormPesel($pesel);
        $resultWinner->setCreatedAt(new \DateTime());

        $clause1        = ($clause1 == 'on') ? true : false;
        $secondAddress  = ($secondAddress == 'on') ? true : false;

        $resultWinner->setFormClause1($clause1);

        if($secondAddress){
            $resultWinner->setFormSecondAddress(true);
            $resultWinner->setFormStreet2($street2);
            $resultWinner->setFormHnumber2($hnumber2);
            $resultWinner->setFormFlat2($flat2);
            $resultWinner->setFormZipCode2($zipCode2);
            $resultWinner->setFormCity2($city2);
        }

        $dm->flush();

        $result['success'] = true;

        $response->setData($result);
        return $response;
    }

    public function checkIfCorrectHash($hash = ''){

        if($hash == '')
            return false;

        $dm             =  $this->get('doctrine_mongodb')->getManager();
        $resultWinner    = $dm->getRepository($this->getParameter('winner_form.winner_document_name'))->findOneBy(array('hash' => $hash));

        if(!$resultWinner)
            return false;

        if($resultWinner->getFilledData() == true){
            return false;
        }else {
            return true;
        }
    }
}
