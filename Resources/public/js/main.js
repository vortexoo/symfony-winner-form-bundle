
var optimizer = {};

optimizer.isMobile = (function() {
    return navigator.userAgent.match(/Android|AvantGo|BlackBerry|DoCoMo|Fennec|iPod|iPhone|iPad|J2ME|MIDP|NetFront|Nokia|Opera Mini|Opera Mobi|PalmOS|PalmSource|portalmmm|Plucker|ReqwirelessWeb|SonyEricsson|Symbian|UP\\.Browser|webOS|Windows CE|Windows Phone OS|Xiino/i);
})();

optimizer.browser = (function() {
    var ua = navigator.userAgent;

    ua = ua.toLowerCase();

    var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
        /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
        /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
        /(msie) ([\w.]+)/.exec( ua ) || /(trident)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
        ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
        [];

    if(match[1] == 'trident')
        match[1] = 'msie';

    return {
        name: match[1] || "",
        version: match[2] || "0"
    };
})();


(function() {

    if(optimizer.browser.name == 'msie' && optimizer.browser.version <= 8.0) {
        var code = '<div id="browser"><div class="wrap"><h2>Spokojnie!</h2><p class="desc">By znaczenie nadać słowom, <i>przeglądarkę pobierz nową.</i> </p><div class="choose"><a class="safari" target="_blank" href="http://www.apple.com/safari/"><span class="image"></span><span class="title">Apple Safari</span><span class="btn">Pobierz</span></a><a class="ie" target="_blank" href="http://windows.microsoft.com/ie"><span class="image"></span><span class="title">Internet Explorer</span><span class="btn">Pobierz</span></a><a class="chrome" target="_blank" href="http://www.google.com/chrome"><span class="image"></span><span class="title">Google Chrome</span><span class="btn">Pobierz</span></a><a class="firefox" target="_blank" href="http://www.firefox.com/"><span class="image"></span><span class="title">Mozilla Firefox</span><span class="btn">Pobierz</span></a></div></div></div>';

        document.body.innerHTML = code;

        return;
    }

})();

$(document).ready(function() {

    var formData;

    $('input[type="checkbox"], input[type="radio"]').ezMark();

    $('input[id="delivery_address"]').click(function() {
        $('.delivery_address').slideToggle();
    });

    $.validator.addMethod("phonePL", function(value, element) {
        var isValid = value.match(/^(\+?[0-9]{2})? ?((0\-[0-9]{2})? ?[0-9]{2} ?[0-9]{2} ?[0-9]{3}$)|([0-9]{3} ?[0-9]{3} ?[0-9]{3}$)/);
        return isValid;
    }, "Podaj poprawny numer telefonu kontaktowego");

    $.validator.addMethod("zipcode", function(value, element) {
        var isValid = value.match('^[0-9]{2}\-?[0-9]{3}$');
        return isValid;
    }, "Podaj poprawny kod pocztowy");

    $.validator.addMethod("delivery_zipcode", function(value, element) {
        var isValid = value.match('^[0-9]{2}\-?[0-9]{3}$');
        return isValid;
    }, "Podaj poprawny kod pocztowy");


    $("#winner-form form").validate({
        errorElement: 'div',
        errorClass: 'error-message',
        rules: {
            firstname: {
                required: true
            },
            surname: {
                required: true,
                minlength: 2
            },
            street: {
                required: true,
                minlength: 2
            },
            zipcode: {
                required: true,
                zipcode: true
            },
            city: {
                required: true
            },
            hnumber: {
                required: true
            },

            delivery_street: {
                required: true,
                minlength: 2
            },
            delivery_zipcode: {
                required: true,
                zipcode: true
            },
            delivery_city: {
                required: true
            },
            delivery_hnumber: {
                required: true
            },

            tel: {
                required: true,
                phonePL: true
            },
            usn: {
                required: true,
                minlength: 2
            },
            usadr: {
                required: true,
                minlength: 2
            },
            peselnip: {
                required: true,
                minlength: 10
            },
            bank: {
                required: true,
                minlength: 16
            },
            agree: {
                required: true
            }
        },
        messages: {
            firstname: 'Podaj imię',
            surname: 'Podaj swoje nazwisko',
            street: 'Podaj ulicę zamieszkania',
            hnumber: 'Podaj numer domu',
            zipcode: {
                required: 'Podaj poprawny kod pocztowy'

            },
            city: 'Podaj nazwę miejscowości',

            delivery_street: 'Podaj ulicę dostarczenia nagrody',
            delivery_hnumber: 'Podaj numer domu',
            delivery_zipcode: {
                required: 'Podaj poprawny kod pocztowy'

            },
            delivery_city: 'Podaj nazwę miejscowości',

            tel: {
                required: 'Podaj numer telefonu kontaktowego'
            },
            usn: 'Uzupełnij pole',
            usadr: 'Uzupełnij pole',
            peselnip: 'Uzupełnij pole',
            bank: 'Uzupełnij pole',
            agree: 'Wyraź zgodę na przetwarzanie danych'
        },
        errorPlacement: function(error, element) {
            if (element.is(':checkbox')) {
                error.appendTo(element.parent().parent());
            }
            else {
                error.appendTo(element.parent());
            }
        },
        highlight: function(element) {
            $(element).closest('.input').addClass("error");
            if($(element).closest('.hnumber').length)
                $(element).closest('.hnumber').next('.flat').addClass('serror');
        },
        unhighlight: function(element) {
            $(element).closest('.input').removeClass("error");
            if($(element).closest('.hnumber').length)
                $(element).closest('.hnumber').next('.flat').removeClass('serror');
        }
    });


    $('.next').on('click',function(e){
        e.preventDefault();

        if($('#winner-form form').valid()){

            $('#step2 .container').css('visibility', 'visible');

            $('#sliders').animate({'margin-left':'-100%'},500, function(){

                $('html,body').animate({ scrollTop: 0 }, 'normal');
                $('#winner-form #step1 .container').css('visibility', 'hidden');
            });

            formData = $('#winner-form form').serialize();

            var step = 0;
            $('#step2 dl dd').each(function(i) {

//                i = i + step;
                var value = $('form input').eq(i+step).val();

                if(i == 7 && $('.delivery_address').length)
                {
                    checked = $('form input').eq(i+step).prop('checked');
                    $(this).html(checked ? 'Tak' : 'Nie');
                    if(!checked)
                        $('.delivery_check').hide();
                    else
                        $('.delivery_check').show();

                }
                else if(i==3){
                    var sep = $('form .input input[type=text]').eq(i+1).val() != '' ? ' / ' : ' ';
                    $(this).html(value+sep+$('form .input input[type=text]').eq(i+1).val());
                    step = 1;

                }
                else if(i==9 && $('.delivery_address').length){
                    var sep = $('form .input input[type=text]').eq(i+1).val() != '' ? ' / ' : ' ';
                    $(this).html(value+sep+$('form .input input[type=text]').eq(i+1).val());
                    step = 2;

                }
                else
                {
                    if(typeof value != "undefined")
                    {
                        $(this).html(value);
                    }
                }

//                else if(i>=4){
//                    $(this).html($('form .input input[type=text]').eq(i+1).val());
//                }
//                else{
//                    $(this).html($('form .input input[type=text]').eq(i).val());
//                }
            });
        }
    });


    $('.prev').on('click',function(e){
        e.preventDefault();
        $('#step1 .container').css('visibility', 'visible');
        $('#sliders').animate({'margin-left':'0'},500, function(){
            $('html,body').animate({ scrollTop: 0 }, 'normal');
            $('#winner-form #step2 .container').css('visibility', 'hidden');
        });
    });


    $('#winner-form input[type=submit]').on('click',function(e){
        e.preventDefault();

        $.ajax({
            type: 'post',
            dataType : 'json',
            url: $('#user_form').attr('action'),
            data: formData,
            success: function(data) {
                if(data.success)
                {
                    $('#step3 .container').css('visibility', 'visible');
                    $('#sliders').animate({'margin-left':'-200%'},500, function(){
                        $('#winner-form #step2 .container').css('visibility', 'hidden');
                        $('html,body').animate({ scrollTop: 0 }, 'normal');
                    });
                }
                else
                {
                    var message = 'Wystąpił nieznany błąd. Spróbuj za chwilę.';
                    if(data.error)
                        message = data.error;
                    $('#form-error').html(message);
                    $('#step1 .container').css('visibility', 'visible');
                    $('#sliders').animate({'margin-left':0},500, function(){
                        $('#winner-form #step2 .container').css('visibility', 'hidden');
                        $('html,body').animate({ scrollTop: 0 }, 'normal');
                    });
                }
            },
            error: function(data) {
                $('#step1 .container').css('visibility', 'visible');
                $('#sliders').animate({'margin-left':0},500, function(){
                    $('html,body').animate({ scrollTop: 0 }, 'normal');
                    $('#winner-form #step2 .container').css('visibility', 'hidden');
                });
            }
        });
    });

});
