# **GONG winner form bundle.** #

It allows you to add quick winner form into your application based on hash provided in email.

## Installation: ##

Modify your composer.json:


```
#!javascript

repositories:
[
...
{
  "type" : "vcs",
  "url" : "https://bitbucket.org/vortexoo/symfony-winner-fom-bundle.git"
}
...
],
require:{
...
"gong/winner-form-bundle" : "dev-master",
...
}
```
Run command in your console:
```
#!bash

composer update gong/winner-form-bundle
```


Paste into your routing.yml file:

```
#!php

gong_winner_form:
    resource: "@WinnerFormBundle/Resources/config/routing.yml"
    prefix:   /
```

Update your AppKernel.php:

```
#!php

$bundles = array(
    ...
    new \Gong\WinnerFormBundle\WinnerFormBundle(),
    ...
);
```

Edit your config.yml file:


```
#!yml

winner_form:
      winner_document_name: "AppBundle:Winner" //name of your document containing winner data and hash
```

Install assets:
```
#!bash

php app/console assets:install
```

Example **Winner** document (don't forget to reference it with your answer document):

```
#!php
<?php

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Indexes;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Index;


/**
 * @MongoDB\Document(repositoryClass="AppBundle\Repository\WinnerRepository")
 * @MongoDB\HasLifecycleCallbacks
 * @Indexes({
 *   @Index(keys={"hash"="asc"})
 * })
 */
class Winner
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\Boolean
     */
    protected $filled_data = false;

    /**
     * @MongoDB\Boolean
     */
    protected $mail_sent = false;

    /**
     * @MongoDB\Date
     */
    protected $won_at;

    /**
     * @MongoDB\String
     */
    public $name = '';

    /**
     * @MongoDB\String
     */
    protected $email = '';

    /**
     * @MongoDB\String
     */
    protected $city = '';

    /**
     * @MongoDB\String
     */
    protected $hash = '';

    /**
     * @MongoDB\String
     */
    protected $form_first_name = '';

    /**
     * @MongoDB\String
     */
    protected $form_last_name = '';

    /**
     * @MongoDB\String
     */
    protected $form_street = '';

    /**
     * @MongoDB\String
     */
    protected $form_hnumber = '';

    /**
     * @MongoDB\String
     */
    protected $form_flat = '';

    /**
     * @MongoDB\String
     */
    protected $form_zip_code = '';

    /**
     * @MongoDB\String
     */
    protected $form_city = '';

    /**
     * @MongoDB\String
     */
    protected $form_phone = '';

    /**
     * @MongoDB\Boolean
     */
    protected $form_second_address = false;

    /**
     * @MongoDB\String
     */
    protected $form_street2 = '';

    /**
     * @MongoDB\String
     */
    protected $form_hnumber2 = '';

    /**
     * @MongoDB\String
     */
    protected $form_flat2 = '';

    /**
     * @MongoDB\String
     */
    protected $form_zip_code2 = '';

    /**
     * @MongoDB\String
     */
    protected $form_city2 = '';

    /**
     * @MongoDB\String
     */
    protected $form_revenue = '';

    /**
     * @MongoDB\String
     */
    protected $form_revenue_address = '';

    /**
     * @MongoDB\String
     */
    protected $form_pesel = '';

    /**
     * @MongoDB\Boolean
     */
    protected $form_clause1 = false;

    /**
     * @MongoDB\Date
     */
    protected $created_at;

    /**
     * @MongoDB\Date
     */
    protected $updated_at;

    /**
     * @MongoDB\PrePersist()
     */
    public function preNew()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * @MongoDB\PreUpdate()
     */
    public function preUpdate()
    {
        $this->setUpdatedAt(new \DateTime());
    }
}
```

You can test your form here:


```
#!php

http://localhost:8000/formularz/[your_hash]
```

If hash exists in your collection and data has not been filled, it will show winner form, else it will redirect you to your homepage.